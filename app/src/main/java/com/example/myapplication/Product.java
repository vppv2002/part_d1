package com.example.myapplication;

public class Product {
    private String manufacturer;
    private String address;
    private double price;

    public Product(String manufacturer, String address, double price) {
        this.manufacturer = manufacturer;
        this.address = address;
        this.price = price;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getAddress() {
        return address;
    }

    public double getPrice() {
        return price;
    }
}