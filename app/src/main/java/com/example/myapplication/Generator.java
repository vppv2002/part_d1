package com.example.myapplication;

import java.util.ArrayList;
import java.util.List;

public final class Generator {
    private Generator() {
    }

    public static List<Product> generate() {
        List<Product> list = new ArrayList<>();
        list.add(new Product("iPhone XS Max", "г.Харьков, ул. Сумская, 17", 23500d));
        list.add(new Product("Samsung S20", "г.Харьков, ул. Героев труда, 72", 25000d));
        list.add(new Product("Xiaomi Redmi 6", "г.Харьков, Академика Павлова, 5", 13000d));
        list.add(new Product("iPhone XS Max", "г.Харьков, ул. Сумская, 17", 23500d));
        list.add(new Product("Samsung S20", "г.Харьков, ул. Героев труда, 72", 25000d));
        list.add(new Product("Xiaomi Redmi 6", "г.Харьков, Академика Павлова, 5", 13000d));
        list.add(new Product("iPhone XS Max", "г.Харьков, ул. Сумская, 17", 23500d));
        list.add(new Product("Samsung S20", "г.Харьков, ул. Героев труда, 72", 25000d));
        list.add(new Product("Xiaomi Redmi 6", "г.Харьков, Академика Павлова, 5", 13000d));
        list.add(new Product("iPhone XS Max", "г.Харьков, ул. Сумская, 17", 23500d));
        list.add(new Product("Samsung S20", "г.Харьков, ул. Героев труда, 72", 25000d));
        list.add(new Product("Xiaomi Redmi 6", "г.Харьков, Академика Павлова, 5", 13000d));
        list.add(new Product("iPhone XS Max", "г.Харьков, ул. Сумская, 17", 23500d));
        list.add(new Product("Samsung S20", "г.Харьков, ул. Героев труда, 72", 25000d));
        list.add(new Product("Xiaomi Redmi 6", "г.Харьков, Академика Павлова, 5", 13000d));
        list.add(new Product("iPhone XS Max", "г.Харьков, ул. Сумская, 17", 23500d));
        list.add(new Product("Samsung S20", "г.Харьков, ул. Героев труда, 72", 25000d));
        list.add(new Product("Xiaomi Redmi 6", "г.Харьков, Академика Павлова, 5", 13000d));
        list.add(new Product("iPhone XS Max", "г.Харьков, ул. Сумская, 17", 23500d));
        list.add(new Product("Samsung S20", "г.Харьков, ул. Героев труда, 72", 25000d));
        list.add(new Product("Xiaomi Redmi 6", "г.Харьков, Академика Павлова, 5", 13000d));
        list.add(new Product("iPhone XS Max", "г.Харьков, ул. Сумская, 17", 23500d));
        list.add(new Product("Samsung S20", "г.Харьков, ул. Героев труда, 72", 25000d));
        list.add(new Product("Xiaomi Redmi 6", "г.Харьков, Академика Павлова, 5", 13000d));
        list.add(new Product("iPhone XS Max", "г.Харьков, ул. Сумская, 17", 23500d));
        list.add(new Product("Samsung S20", "г.Харьков, ул. Героев труда, 72", 25000d));
        list.add(new Product("Xiaomi Redmi 6", "г.Харьков, Академика Павлова, 5", 13000d));

        return list;
    }
}
